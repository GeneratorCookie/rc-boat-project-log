# RC Boat Project Log

# Entries

## 02/23/2020

1. Purchased a [2.4GHz 3 channel receiver](https://www.amazon.com/gp/product/B077N5TQTB) that's also waterproof to go on the boat, but I don't have a transmitter yet
1. Thinking about building my own transmitter using a PS4 controller as the user input; would be kind of cool to use the IMU onboard the PS4 controller to control the boat
1. Seems pretty easy to connect the PS4 controller to the Arduino ([here](https://www.instructables.com/id/Creating-a-DualShock-4-Controlled-Arduino/))
1. Something that I should be thinking about while designing this is a mechanical kill switch that disconnects the batteries from the motors. Could be a separate receiver listening to a separate channel. This is going to be important once I start automating the boat
1. Potential 2.4G transmitter for the Arduino [here](https://www.amazon.com/dp/B07L8RXSDF)
1. How to use the transmitter with the Arduino [here](https://howtomechatronics.com/tutorials/arduino/arduino-wireless-communication-nrf24l01-tutorial/)